﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

class SpeedTest
{
	static void testManagedBitmap()
	{
        int w = 4321;
		int stride = (w + 3) & ~3;
		int h = 6789;
        var img = Marshal.AllocHGlobal(sizeof(byte) * h * stride);

        var dst = img;
        
        var a = new byte[w];
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < a.Length; x++)
			{
				var val = x ^ y;
				var v = (byte)val;
				a[x] = v;
			}

			Marshal.Copy(a, 0, dst, a.Length);
			dst = IntPtr.Add(dst, sizeof(byte)*stride);
		}

        Marshal.FreeHGlobal(img);
	}

	static void testManagedOpt()
    {
        uint w = 4321;
        uint stride = (w + 3u) & ~3u;
        uint h = 6789;
        var a = new byte[stride * h];

        int x = 0;
        int y = 0;
        for (int idx = 0; idx < a.Length; idx++)
        {
            var val = x ^ y;
            var v = (byte)val;

            a[idx] = v;

            if (x == stride - 1)
            {
                x = -1;
                y++;
            }
            x++;
        }
    }

    static void test1()
    {
        int w = 4321;
        int stride = (w + 3) & ~3;
        int h = 6789;
        var a = new byte[stride * h];
        for (int y = 0; y < h; y++)
        {
            int offset = y * stride;
            for (int x = 0; x < w; x++)
            {
                var val = x ^ y;
                var v = (byte)val;
                var dst = x + offset;
                a[dst] = v;
            }
        }
    }

    static void test2()
    {
        int w = 4321;
        int stride = (w + 3) & ~3;
        int h = 6789;
        var a = new byte[stride * h];
        unsafe
        {
            fixed (byte* p0 = a)
            {
                for (int y = 0; y < h; y++)
                {
                    byte* p = p0 + y * stride;
                    for (int x = 0; x < w; x++)
                    {
                        var val = x ^ y;
                        var v = (byte)val;
                        *p = v;
                        p++;
                    }
                }
            }
        }
    }

    static void time(Action action, int count = 100)
    {
        var tw = new Stopwatch();
        tw.Start();
        for (int i = 0; i < count; i++)
            action();
        tw.Stop();
        Console.WriteLine("{0} : {1}[ms]", action.Method.Name, tw.ElapsedMilliseconds);
    }

    static void Main(string[] args)
	{
		time(testManagedBitmap);
        time(testManagedOpt);
        time(test1);
        time(test2);
    }
}
